use components::card::Card;
use yew::prelude::*;

mod components;

enum Msg {}

struct Home {}

impl Component for Home {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn update(&mut self, _ctx: &Context<Self>, _msg: Self::Message) -> bool {
        false
    }

    fn view(&self, _ctx: &Context<Self>) -> Html {
        html! {
            <main>
                <Card />
            </main>
        }
    }
}

fn main() {
    yew::start_app::<Home>();
}
