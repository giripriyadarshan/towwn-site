use yew::{html, Component, Context, Html};

use material_yew::{
    button::MatButton,
    text_inputs::{MatTextField, TextFieldType},
};

pub enum PhoneNumberInput {
    PhoneNumber(String),
    GoToWhatsapp,
}

pub struct Card {
    number: String,
    valid: bool,
}

impl Component for Card {
    type Message = PhoneNumberInput;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            number: String::new(),
            valid: false,
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            PhoneNumberInput::PhoneNumber(number) => {
                if number.len() == 10 {
                    self.valid = true;
                    self.number = number;
                    true
                } else {
                    self.valid = false;
                    false
                }
            }
            PhoneNumberInput::GoToWhatsapp => {
                if self.valid {
                    let url = format!(
                        "https://api.whatsapp.com/send/?phone=91{}&text&app_absent=0",
                        self.number
                    );
                    web_sys::window().unwrap().open_with_url(&url).unwrap();
                    true
                } else {
                    false
                }
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let oninputnumber = ctx
            .link()
            .callback(|e: String| PhoneNumberInput::PhoneNumber(e));

        let onclick = ctx.link().callback(|_| PhoneNumberInput::GoToWhatsapp);

        html! {
            <div class="card">
            <MatTextField
                outlined=true
                label="Phone Number"
                field_type={TextFieldType::Tel}
                max_length={10}
                pattern="[0-9]*"
                auto_validate=true
                value={self.number.clone()}
                oninput={oninputnumber}
                />

                <a onclick={onclick}>
                    <MatButton
                        label="Go To Text"
                        outlined=true
                        />
                </a>

            </div>
        }
    }
}
